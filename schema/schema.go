package schema

import (
	"github.com/graphql-go/graphql"
)

// CreateSchema creates schema
func CreateSchema() (*graphql.Schema, error) {
	schema, err := graphql.NewSchema(
		graphql.SchemaConfig{
			Query: graphql.NewObject(
				graphql.ObjectConfig{
					Name: "query",
					Fields: graphql.Fields{
						"account":  accountField,
						"accounts": accountsField,
						"user":     userField,
						"users":    usersField,
					},
				},
			),
			Subscription: nil,
			Types:        []graphql.Type{},
			Directives:   []*graphql.Directive{},
		},
	)

	return &schema, err
}
