package schema

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/burkostya/testql/models"
)

var userType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"account": accountField,
		},
	},
)

var userField = &graphql.Field{
	Type: userType,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		id := params.Args["id"].(string)

		user := models.Users[id]

		return user, nil
	},
}

var usersField = &graphql.Field{
	Type: graphql.NewList(userType),
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		var users []models.User

		for _, u := range models.Users {
			users = append(users, u)
		}

		return users, nil
	},
}
