package schema

import (
	"github.com/graphql-go/graphql"
	"github.com/pkg/errors"
	"gitlab.com/burkostya/testql/models"
)

var accountType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Account",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.Int,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
	},
})

func init() {
	accountType.AddFieldConfig("users", usersField)
}

// AccountLinker has link to account
type AccountLinker interface {
	AccountID() string
}

var accountField = &graphql.Field{
	Type: accountType,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Type: graphql.String,
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		id, exists := params.Args["id"].(string)
		if !exists {
			if linker, ok := params.Source.(AccountLinker); ok {
				id = linker.AccountID()
			}
		}
		if id == "" {
			return nil, errors.New("id of account was not provided")
		}

		account := models.Accounts[id]

		return account, nil
	},
}

var accountsField = &graphql.Field{
	Type: graphql.NewList(accountType),
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {

		var accounts []models.Account

		for _, acc := range models.Accounts {
			accounts = append(accounts, acc)
		}

		return accounts, nil
	},
}
