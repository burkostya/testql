package main

import (
	"log"
	"net/http"

	"github.com/graphql-go/handler"
	"gitlab.com/burkostya/testql/schema"
)

func main() {
	sch, err := schema.CreateSchema()
	if err != nil {
		log.Fatal("GraphQL private schema error: ", err)
	}

	h := handler.New(&handler.Config{
		Schema:   sch,
		Pretty:   true,
		GraphiQL: true,
	})

	http.Handle("/graphql", h)

	addr := "127.0.0.1:3000"
	log.Println("Listen on ", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
