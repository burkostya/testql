package models

// User represents user
type User struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	accountID string
}

// AccountID returns id of account
func (user User) AccountID() string {
	return user.accountID
}

// Users is stub for users
var Users = map[string]User{
	"1": {
		ID:        "1",
		Name:      "user 1",
		accountID: "1",
	},
	"2": {
		ID:        "2",
		Name:      "user 2",
		accountID: "1",
	},
	"3": {
		ID:        "3",
		Name:      "user 3",
		accountID: "2",
	},
	"4": {
		ID:        "4",
		Name:      "user 4",
		accountID: "2",
	},
}
