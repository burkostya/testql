package models

// Account represents account
type Account struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Accounts is stub for accounts
var Accounts = map[string]Account{
	"1": {
		ID:   "1",
		Name: "acc 1",
	},
	"2": {
		ID:   "2",
		Name: "acc 2",
	},
}
