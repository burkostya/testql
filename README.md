# Run

```sh
dep ensure
go run main.go
```

# Test

Send query below to the `http://localhost:3000/graphql`

```graphql
query {
  user(id: "1") {
    name
    account {
      id
      users {
        id
        name
        account {
          name
          users {
            name
          }
        }
      }
    }
  }
}
```
